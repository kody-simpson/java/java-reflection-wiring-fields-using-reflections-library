package me.kodysimpson;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //Setup the Reflections object from their imported library and configure
        //it with the field annotation scanner and project package.
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("me.kodysimpson"))
                .setScanners(new FieldAnnotationsScanner()));

        //Get all fields annotated with our custom annotation
        Set<Field> fields = reflections.getFieldsAnnotatedWith(Wire.class);

        //instantiate this object so we can do stuff with it
        FoodController foodController = new FoodController();

        //loop through all located fields annotated with Wire
        for (Field field : fields){

            //Get field type class (FoodService)
            Class typeClass = field.getType();
            //get it's constructor
            Constructor constructor = typeClass.getConstructor();
            //invoke it to make an object
            Object object = constructor.newInstance();

            //wire the field with the created object
            field.setAccessible(true);
            field.set(foodController, object);

        }

        //call this method, if there are no exceptions then we know it worked!
        foodController.handleCooking();

    }
}

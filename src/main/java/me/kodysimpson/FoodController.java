package me.kodysimpson;

public class FoodController {

    //we will leave this field to be instantiated using reflection
    @Wire
    private FoodService foodService;

    public void handleCooking(){
        foodService.cookFood();
    }

}
